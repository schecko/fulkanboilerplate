
#include <vulkan/vulkan.h>

#ifndef ASSERT
#include <cassert>
#define ASSERT(test, message) assert(test)
#endif

int countNullTerminated(const char** in) {
	int count = 0;
	if(!in) return count;
	while(*in) {
		count++;
		in++;
	}
	return count;
}

VkInstance NewVkInstance(const char* appName, const char** instanceLayers, const char** instanceExtensions) {
	VkResult error;
	VkInstance instance = NULL;

	VkApplicationInfo aInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
	aInfo.pApplicationName = appName;
	aInfo.pEngineName = appName;
	aInfo.apiVersion = VK_MAKE_VERSION(1, 1, VK_HEADER_VERSION);
	aInfo.applicationVersion = 0;

	VkInstanceCreateInfo iInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
	iInfo.pApplicationInfo = &aInfo;

    iInfo.ppEnabledLayerNames = instanceLayers;
    iInfo.enabledLayerCount = countNullTerminated(instanceLayers);

    iInfo.ppEnabledExtensionNames = instanceExtensions;
    iInfo.enabledExtensionCount = countNullTerminated(instanceExtensions);

	error = vkCreateInstance(&iInfo, NULL, &instance);
	ASSERT(error == VK_SUCCESS, "Could not create vulkan instance");

	return instance;
}


